package jwt

import (
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
)

// RS256Signer signs JWT tokens using the RS256 algorithm.
type RS256Signer struct {
	privateKey *rsa.PrivateKey
}

var _ Signer = &RS256Signer{}

// NewRS256Signer creates a new RS256Signer with the provided RSA Private
// Key.
func NewRS256Signer(privateKey *rsa.PrivateKey) *RS256Signer {
	return &RS256Signer{
		privateKey: privateKey,
	}
}

// Algorithm returns RS256.
func (s *RS256Signer) Algorithm() Algorithm {
	return RS256
}

// Sign signs the provided serialized header and body.
func (s *RS256Signer) Sign(b64HeaderAndBody string) ([]byte, error) {
	hashArr := sha256.Sum256([]byte(b64HeaderAndBody))
	hash := hashArr[:]

	return rsa.SignPKCS1v15(rand.Reader, s.privateKey, crypto.SHA256, hash)
}
