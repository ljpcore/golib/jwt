package jwt

import (
	"crypto/ed25519"
)

// EdDSAVerifier verifies JWT tokens using the EdDSA algorithm.
type EdDSAVerifier struct {
	publicKey ed25519.PublicKey
}

var _ Verifier = &EdDSAVerifier{}

// NewEdDSAVerifier creates a new EdDSAVerifier with the provided Ed25519 Public
// Key.
func NewEdDSAVerifier(publicKey ed25519.PublicKey) *EdDSAVerifier {
	return &EdDSAVerifier{
		publicKey: publicKey,
	}
}

// Verify verifies the provided serialized header and body against the provided
// signature.
func (v *EdDSAVerifier) Verify(b64HeaderAndBody string, signature []byte) bool {
	return ed25519.Verify(v.publicKey, []byte(b64HeaderAndBody), signature)
}
