![](icon.png)

# jwt

Package jwt is a partial but production-ready implementation of RFC-7519.

## Usage Example

The below example demonstrates how to create, sign, serialize, parse and then
verify a JWT token.  The example uses ES256 - but this package also supports
RS256 and EdDSA (Ed25519).

```go
token := jwt.NewToken()
token.AddClaim("uid", "1234")
token.AddScope("read:profile")
token.AddScope("write:profile")

privateKey, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
if err != nil {
	return err
}

signer := jwt.NewES256Signer(privateKey)
if err := token.Sign(signer); err != nil {
	return err
}

tokenStr, err := token.Serialize()
if err != nil {
	return err
}

fmt.Printf("%v\n", tokenStr) // eyJhbGci...

token, err = jwt.Parse(tokenStr)
if err != nil {
	return err
}

verifier := jwt.NewES256Verifier(&privateKey.PublicKey)
fmt.Printf("%v\n", token.Verify(verifier)) // true
```

---

Icons made by [Freepik](https://www.flaticon.com/authors/freepik) from
[www.flaticon.com](https://www.flaticon.com/).

