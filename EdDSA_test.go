package jwt

import (
	"crypto/ed25519"
	"crypto/rand"
	"testing"

	"gitlab.com/ljpcore/golib/test"
	"gitlab.com/ljpcore/golib/test/is"
)

func TestTokenSignAndVerifyEdDSA(t *testing.T) {
	// Arrange.
	publicKey, privateKey, err := ed25519.GenerateKey(rand.Reader)
	test.That(t, err, is.Nil)

	signer := NewEdDSASigner(privateKey)
	verifier := NewEdDSAVerifier(publicKey)

	token := &Token{
		Header: Header{Algorithm: None, Type: "JWT"},
		Body: map[string]interface{}{
			"Key1": "Value1",
			"Key2": "Value2",
		},
	}

	// Act.
	err = token.Sign(signer)
	test.That(t, err, is.Nil)

	valid := token.Verify(verifier)

	// Assert.
	test.That(t, valid, is.True)
}
