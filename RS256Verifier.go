package jwt

import (
	"crypto"
	"crypto/rsa"
	"crypto/sha256"
)

// RS256Verifier verifies JWT tokens using the RS256 algorithm.
type RS256Verifier struct {
	publicKey *rsa.PublicKey
}

var _ Verifier = &RS256Verifier{}

// NewRS256Verifier creates a new RS256Verifier with the provided RSA Public
// Key.
func NewRS256Verifier(publicKey *rsa.PublicKey) *RS256Verifier {
	return &RS256Verifier{
		publicKey: publicKey,
	}
}

// Verify verifies the provided serialized header and body against the provided
// signature.
func (v *RS256Verifier) Verify(b64HeaderAndBody string, signature []byte) bool {
	hashArr := sha256.Sum256([]byte(b64HeaderAndBody))
	hash := hashArr[:]

	return rsa.VerifyPKCS1v15(v.publicKey, crypto.SHA256, hash, signature) == nil
}
