package jwt

import (
	"crypto/rand"
	"crypto/rsa"
	"testing"

	"gitlab.com/ljpcore/golib/test"
	"gitlab.com/ljpcore/golib/test/is"
)

func TestTokenSignAndVerifyRS256(t *testing.T) {
	// Arrange.
	privateKey, err := rsa.GenerateKey(rand.Reader, 2048)
	test.That(t, err, is.Nil)

	signer := NewRS256Signer(privateKey)
	verifier := NewRS256Verifier(&privateKey.PublicKey)

	token := &Token{
		Header: Header{Algorithm: None, Type: "JWT"},
		Body: map[string]interface{}{
			"Key1": "Value1",
			"Key2": "Value2",
		},
	}

	// Act.
	err = token.Sign(signer)
	test.That(t, err, is.Nil)

	valid := token.Verify(verifier)

	// Assert.
	test.That(t, valid, is.True)
}
