package jwt

import (
	"crypto/ed25519"
)

// EdDSASigner signs JWT tokens using the EdDSA algorithm.
type EdDSASigner struct {
	privateKey ed25519.PrivateKey
}

var _ Signer = &EdDSASigner{}

// NewEdDSASigner creates a new EdDSASigner with the provided Ed25519 Private
// Key.
func NewEdDSASigner(privateKey ed25519.PrivateKey) *EdDSASigner {
	return &EdDSASigner{
		privateKey: privateKey,
	}
}

// Algorithm returns EdDSA.
func (s *EdDSASigner) Algorithm() Algorithm {
	return EdDSA
}

// Sign signs the provided serialized header and body.
func (s *EdDSASigner) Sign(b64HeaderAndBody string) ([]byte, error) {
	return ed25519.Sign(s.privateKey, []byte(b64HeaderAndBody)), nil
}
