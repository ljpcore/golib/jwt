package jwt

// Algorithm is an alias for string that defines the set of supported JWT
// signature types.
type Algorithm string

// The supported signature types.
const (
	None  Algorithm = "None"
	RS256 Algorithm = "RS256"
	ES256 Algorithm = "ES256"
	EdDSA Algorithm = "EdDSA"
)
