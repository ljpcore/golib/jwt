package jwt

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"testing"

	"gitlab.com/ljpcore/golib/test"
	"gitlab.com/ljpcore/golib/test/is"
)

func TestTokenSignAndVerifyES256(t *testing.T) {
	// Arrange.
	privateKey, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	test.That(t, err, is.Nil)

	signer := NewES256Signer(privateKey)
	verifier := NewES256Verifier(&privateKey.PublicKey)

	token := &Token{
		Header: Header{Algorithm: None, Type: "JWT"},
		Body: map[string]interface{}{
			"Key1": "Value1",
			"Key2": "Value2",
		},
	}

	// Act.
	err = token.Sign(signer)
	test.That(t, err, is.Nil)

	valid := token.Verify(verifier)

	// Assert.
	test.That(t, valid, is.True)
}
